﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace PubAdSample.Common
{
    public class BasicPage : Page
    {
        public BasicPage()
        {
            Loaded += BasicPage_Loaded;
        }

        void BasicPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            CommandBar commandBar = new CommandBar();
            commandBar.Background = new SolidColorBrush(Windows.UI.Colors.LightGray);
            commandBar.BorderThickness = new Thickness(0, 10, 0, 0);

            // Create the Back button.
            AppBarButton backButton = new AppBarButton();
            backButton.Icon = new SymbolIcon(Symbol.Back);
            backButton.Label = "Back";
            backButton.Click += backButton_Click;

            //Create Refresh Button
            AppBarButton refreshButton = new AppBarButton();
            refreshButton.Icon = new SymbolIcon(Symbol.Refresh);
            refreshButton.Label = "Refresh";
            refreshButton.Click += refreshButton_Click;


            // Add the buttons to the command bar.
            commandBar.PrimaryCommands.Add(backButton);
            commandBar.PrimaryCommands.Add(new AppBarSeparator());
            commandBar.PrimaryCommands.Add(refreshButton);

            this.BottomAppBar = commandBar;
        }

        public void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        public virtual void refreshButton_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
