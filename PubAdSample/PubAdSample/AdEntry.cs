﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubAdSample
{
    public class AdEntry
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri ImageUri { get; set; }
        public String Tag { get; set;
        }
        public AdEntry(String t, String d, String tg)
        {
            Title = t;
            Description = d;
            Tag = tg;
           // ImageUri = u;
        }


    }
}
