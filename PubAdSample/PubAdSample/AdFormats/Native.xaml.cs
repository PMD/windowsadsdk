﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MASTAdView;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;
using MASTNativeAdView;

namespace PubAdSample.AdFormats
{
    public sealed partial class Native : Page
    {
        private String AD_URL = "http://ads.mocean.mobi/ad";

        public Native()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private List<AssetRequest> getAssetRequests()
        {
            List<AssetRequest> assets = new List<AssetRequest>();

            TitleAssetRequest titleAsset = new TitleAssetRequest();
            titleAsset.SetAssetId(1); // Unique assetId is mandatory for each asset
            titleAsset.SetLength(50);
            titleAsset.SetRequired(true); // Optional (Default: false)

            assets.Add(titleAsset);

            ImageAssetRequest imageAssetIcon = new ImageAssetRequest();
            imageAssetIcon.SetAssetId(2);
            imageAssetIcon.SetImageType(ImageAssetTypes.Icon);
            imageAssetIcon.SetWidth(60); // Optional
            imageAssetIcon.SetHeight(60); // Optional

            assets.Add(imageAssetIcon);

            ImageAssetRequest imageAssetLogo = new ImageAssetRequest();
            imageAssetLogo.SetAssetId(3);
            imageAssetLogo.SetImageType(ImageAssetTypes.Logo);
            assets.Add(imageAssetLogo);

            ImageAssetRequest imageAssetMainImage = new ImageAssetRequest();
            imageAssetMainImage.SetAssetId(4);
            imageAssetMainImage.SetImageType(ImageAssetTypes.Main);
            assets.Add(imageAssetMainImage);

            DataAssetRequest dataAssetDesc = new DataAssetRequest();
            dataAssetDesc.SetAssetId(5);
            dataAssetDesc.SetDataAssetType(DataAssetTypes.desc);
            dataAssetDesc.SetLength(25);
            assets.Add(dataAssetDesc);

            DataAssetRequest dataAssetRating = new DataAssetRequest();
            dataAssetRating.SetAssetId(6);
            dataAssetRating.SetDataAssetType(DataAssetTypes.rating);
            assets.Add(dataAssetRating);

            return assets;
        }

        private void Button_Load_Ad(object sender, RoutedEventArgs e)
        {
            // Initialize the adview
            MASTNativeAd ad = new MASTNativeAd(this);
            ad.SetRequestListener(new AdRequestListener(this));
            ad.SetZone(179492); // TODO: Add your ZoneId

            // Request for native assets
            ad.AddNativeAssetRequestList(getAssetRequests());

            // Set to use in app browser
            ad.SetUseInternalBrowser(true);

            // Add some custom parameters
            ad.AddCustomParameter("keywords", "NFL,Football,Sports,Games,WordsCup");
            ad.AddCustomParameter("isp", "T-mobile");
            ad.AddCustomParameter("age", "25");
            ad.AddCustomParameter("gender", "m");
            ad.AddCustomParameter("androidid", "testandroidid1234");
            ad.AddCustomParameter("country", "US");
            ad.AddCustomParameter("city", "New York, NY");

            ad.SetAdNetworkURL(AD_URL);

            // ad.setTest(true);

            // This is by default false.
            //ad.overrideAdapterLoading(false);

            /*
             * Add your device id for Facebook Audience network to get test ads.
             * This will be printed in the logs once you launch the application for
             * the first time.
             */
            //ad.addTestDeviceIdForNetwork(
            //MediationNetwork.FACEBOOK_AUDIENCE_NETWORK, "HASHED_ID");
            // TODO: Add your test device hash id above

            ad.Update();
        }

        private class AdRequestListener : MASTNativeAd.INativeRequestListener
        {
            Native context;

            public AdRequestListener(Native context)
            {
                this.context = context;
            }
          
            async void MASTNativeAd.INativeRequestListener.OnNativeAdReceived(MASTNativeAd ad)
            {
                if (ad != null)
                {
                    List<AssetResponse> nativeAssets = ad.GetNativeAssets();

                    for (int i = 0; i < nativeAssets.Count; i++)
                    {
                        AssetResponse asset = nativeAssets.ElementAt(i);

                        if (asset is TitleAssetResponse)
                        {
                            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                            () =>
                            {
                                context.nativeTitle.Text = ((TitleAssetResponse)asset).GetTitleText();
                            });
                            continue;
                        }
                        else if (asset is ImageAssetResponse)
                        {
                            switch (((ImageAssetResponse)asset).GetImageType())
                            {
                                case ImageAssetTypes.Icon:
                                    MASTNativeAd.Image iconImage = ((ImageAssetResponse)asset).GetImage();
                                    if (iconImage != null)
                                    {
                                        // Set Icon Image
                                        await ad.LoadImage(iconImage.GetUrl(), context.icon_image);
                                    }
                                    break;

                                case ImageAssetTypes.Logo:
                                    // Code to render logo image ...
                                    break;
                                case ImageAssetTypes.Main:
                                    MASTNativeAd.Image mainImage = ((ImageAssetResponse)asset).GetImage();
                                    if (mainImage != null)
                                    {
                                        // Set Main Image
                                        //await ImageDownloader.loadImage(mainImage.getUrl(),context.main_mage);
                                    }
                                    break;
                            }

                            continue;

                        }
                        else if (asset is DataAssetResponse)
                        {
                            switch (((DataAssetResponse)asset).GetDataAssetType())
                            {
                                case DataAssetTypes.desc:
                                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                                    () =>
                                    {
                                        // context.nativeDescription.Text = ((DataAssetResponse)asset).getValue();
                                    });
                                    break;
                                case DataAssetTypes.ctatext:
                                    // Code to render CTA text/button
                                    break;
                                case DataAssetTypes.rating:
                                    // Set Rating
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    ad.RegisterTrackersForControl(context.NativeAdContainer);
                }
            }

            void MASTNativeAd.INativeRequestListener.OnNativeAdFailed(MASTNativeAd ad, Exception ex)
            {
                // throw new NotImplementedException();
            }

            void MASTNativeAd.INativeRequestListener.OnReceivedThirdPartyRequest(MASTNativeAd ad, Dictionary<string, string> properties, Dictionary<string, string> parameters)
            {
                // throw new NotImplementedException();
            }

            void MASTNativeAd.INativeRequestListener.OnNativeAdClicked(MASTNativeAd ad)
            {
                // throw new NotImplementedException();
            }
        }
    }
}

