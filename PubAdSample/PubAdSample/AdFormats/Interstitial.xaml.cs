﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using PubAdSample.Common;
// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PubAdSample.AdFormats
{
    public sealed partial class Interstitial : BasicPage
    {
        public Interstitial()
        {
            System.Diagnostics.Debug.WriteLine("Constructor called ");
            this.InitializeComponent();
            pageText.Text = "Some random app page displaying random app data. It will not be visible until interstitial has been dismissed or tapped.";
        }

        override
        public void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            adView.Update(true);
        }
    }
}
