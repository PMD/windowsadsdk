﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using PubAdSample.Common;

namespace PubAdSample.AdFormats
{
    public sealed partial class RichMedia : BasicPage
    {
        public RichMedia()
        {
            this.InitializeComponent();
        }

        override
        public void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            adView.Update(true);
        }
    }
}
