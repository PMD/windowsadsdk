﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MASTAdView.video;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PubAdSample.AdFormats
{
    public sealed partial class Video : Page, AdManagerListener
    {
        VAdManager adManager;

        public Video()
        {
            this.InitializeComponent();
            //loadPubMaticAd();

        }

        private void loadPubMaticAd()
        {
            adManager = new VAdManager(this, onAdRecieved, onAdError, onAdEvent, AD_SERVING_PLATFORM.PUBMATIC);

            Dictionary<string, string> args = new Dictionary<string, string>();
            // Don't allow these to be overridden.
            args.Add("ua", "testapp");
            args.Add("adtype", "13");
            args.Add("pubId", "45710");
            args.Add("siteId", "45711");
            args.Add("adId", "272149");
            args.Add("vadFmt", "2");
            args.Add("adType", "13");//13 is for video & 14 for audio
            args.Add("vminl", "10");
            args.Add("vmaxl", "60");
            args.Add("vtype", "linear");

            //adManager.requestAd("http://172.16.9.145/Linear.xml?", args);
            //adManager.requestAd("http://vid.pubmatic.com//AdServer/AdServerServlet?", args);

            adManager.requestAd("http://demo.tremorvideo.com/proddev/vast/vast_inline_linear.xml?", args);

        }

        private void loadMoceanAd()
        {
            adManager = new VAdManager(this, onAdRecieved, onAdError, onAdEvent, AD_SERVING_PLATFORM.MOCEAN);

            Dictionary<string, string> args = new Dictionary<string, string>();
            // Don't allow these to be overridden.
            args.Add("over_18", "3");
            args.Add("ua", "TestApp");
            args.Add("clientIp", "10.6.4.81");
            args.Add("zone", "122994");
            args.Add("vtype", "linear");
            args.Add("adType", "13");//13 is for video & 14 for audio
            adManager.requestAd("http://ads.moceanads.com//ad?", args);
        }

        private void LoadAdBtn_Click(Object sender,
                               RoutedEventArgs e)
        {

            loadPubMaticAd();
        }

        private void GetDurationBtn_Click(Object sender,
                               RoutedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("Media file duartion = " + adManager.getDuration());
        }

        private void GetPositionBtn_Click(Object sender,
                               RoutedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("Media file position = " + adManager.getPosition());
        }

        private void ShowMessage(object sender, RoutedEventArgs e)
        {
            // textV.Content = "Hello World";
        }

        private void adView_ProcessedRichmediaRequest(object sender, MASTAdView.MASTAdView.ProcessedRichmediaRequestEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("URI : " + e.URI + " Handled : " + e.Handled);
        }

        public void onAdError(VAdManager adManager, int errorType, int errorCode)
        {
            //throw new NotImplementedException();
            System.Diagnostics.Debug.WriteLine("Ad failed errorType = " + errorType + ", errorCode = " + errorCode);
        }

        public void onAdRecieved(VAdManager adManager)
        {
            //throw new NotImplementedException();
            VAdParams adParams = new VAdParams();

            adParams.linearInfo = new VideoViewInfo();
            adParams.linearInfo.container = this.VideoContainer;
            adParams.linearInfo.player = null;

            adManager.init(adParams);
        }

        public void onAdEvent(VAdManager adManager, AD_EVENT adEvent)
        {
            switch (adEvent)
            {
                case AD_EVENT.AD_LOADED:

                    System.Diagnostics.Debug.WriteLine("onAdEvent = " + adEvent);
                    adManager.PlayAd();
                    break;

            }
        }

        public void thirdPartyRequest()
        {
            throw new NotImplementedException();
        }

        public void contentPauseRequest()
        {
            this.contentPlayer.Pause();
            System.Diagnostics.Debug.WriteLine("Content Paused");
        }

        public void contentResumeRequest()
        {
            this.VideoContainer.Content = this.contentPlayer;
            this.contentPlayer.Play();
            System.Diagnostics.Debug.WriteLine("Content Resumed");

        }

        public bool overrideCompanionHandeling()
        {
            throw new NotImplementedException();
        }

        public void renderCompanionAds()
        {
            throw new NotImplementedException();
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                ResetPageCache();
            }
        }
        private void ResetPageCache()
        {
            var cacheSize = ((Frame)Parent).CacheSize;
            ((Frame)Parent).CacheSize = 0;
            ((Frame)Parent).CacheSize = cacheSize;
        }

    }
}