﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using System.Reflection;
using Windows.System;
using Windows.UI.Xaml.Media.Imaging;
// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MASTAdView
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class InternalBrowserPage : Page
    {
        public InternalBrowserPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string url = e.Parameter as string;
            if (url != null)
            {
                this.webBrowserControl.Source = new Uri(url);
            }
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            AppBarButton b = sender as AppBarButton;

            switch (b.Tag.ToString())
            {
                case "Back":
                    try
                    {
                        this.webBrowserControl.GoBack();
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debug.WriteLine("No Forward History");
                    }
                    break;
                case "Forward":
                    try
                    {
                        this.webBrowserControl.GoForward();
                    }
                    catch (Exception)
                    {
                        System.Diagnostics.Debug.WriteLine("No Forward History");
                    }
                    break;
                case "Refresh":
                    this.webBrowserControl.Navigate(this.webBrowserControl.Source);
                    break;
                case "Close": this.Frame.GoBack(); break;

            }
        }

        async private void openButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.webBrowserControl.Source == null)
                return;

            await Launcher.LaunchUriAsync(this.webBrowserControl.Source);
        }
    }
}
