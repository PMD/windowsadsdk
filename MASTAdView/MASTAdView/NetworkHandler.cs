﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MASTAdView
{
    class NetworkHandler
    {
        public static void StartWebRequest(string url, Action<HttpWebResponse, Exception> FinishWebRequest)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.BeginGetResponse(result => {

                try
                {
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.EndGetResponse(result);
                    FinishWebRequest(response, null);
                    
                }
                catch (Exception exception)
                {
                    FinishWebRequest(null, exception);
                   
                }

            }, null); // Don't need the state here any more
        }

       
    }
}
