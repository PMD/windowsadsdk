﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Xml;
using System.Diagnostics;
using System.IO;

namespace MASTAdView
{
    // invoked to inform the caller that the request failed due to connectivity, dns, timout, etc...
    public delegate void AdRequestFailed(AdRequest request, Exception exception);

    // invoked to inform the caller that the request failed due to an error response from the ad network
    public delegate void AdRequestError(AdRequest request, string errorCode, string errorMessage);

    // invoked to inform the caller that the request has obtained an ad descriptor from the ad network
    public delegate void AdRequestCompleted(AdRequest request, AdDescriptor adDescriptor);

    public delegate void AdRequestServed(AdRequest request, System.Net.HttpWebResponse response);

    /// <summary>
    /// Represents a one time ad request.  Use the Create method to create and start the ad request.
    /// Once the returned request triggers a callback or is canceled it becomes invalid.
    /// 
    /// The request and response parsing follow the Mocean Ad Server interface found on the following page:
    /// http://developer.moceanmobile.com/Mocean_Ad_Request_API
    /// </summary>
    public sealed class AdRequest : IDisposable
    {
        // creates and starts ad request
        public static AdRequest Create(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters,
            AdRequestCompleted completedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback)
        {
            AdRequest request = new AdRequest(timeout, adServerURL, userAgent, parameters, completedCallback, errorCallback, failedCallback);

            request.Start();

            return request;
        }

        public static AdRequest InitiateRequest(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters,
AdRequestServed requestServedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback)
        {

            //int timeout = Defaults.NETWORK_TIMEOUT_MILLISECONDS;
            //string adServerURL = sdkEndPoints[(int)endPoint];
            AdRequest request = new AdRequest(timeout, adServerURL, userAgent, parameters, requestServedCallback, errorCallback, failedCallback);

            request.SendRequest();

            return request;
        }

        public static AdRequest InitiateRequest(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters, String postString,
AdRequestServed requestServedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback, bool isNative)
        {

            //int timeout = Defaults.NETWORK_TIMEOUT_MILLISECONDS;
            //string adServerURL = sdkEndPoints[(int)endPoint];
            AdRequest request = new AdRequest(timeout, adServerURL, userAgent, parameters, postString, requestServedCallback, errorCallback, failedCallback, isNative);

            request.SendPostRequest();

            return request;
        }

        private int timeout;
        private string requestURL;
        private string userAgent;
        private string postString;
        private bool isNative = false;
        private AdRequestCompleted completedCallback;
        private AdRequestError errorCallback;
        private AdRequestFailed failedCallback;
        private AdRequestServed requestServedCallback;

        private System.Threading.CancellationTokenSource timeoutTokenSource = null;
        private System.Net.HttpWebRequest webRequest = null;

        private AdRequest(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters,
            AdRequestCompleted completedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback)
        {
            this.userAgent = userAgent;
            this.timeout = timeout;
            this.completedCallback = completedCallback;
            this.errorCallback = errorCallback;
            this.failedCallback = failedCallback;

            string requestURL = adServerURL + "?";
            foreach (KeyValuePair<String, String> param in parameters)
            {
                requestURL += WebUtility.UrlEncode(param.Key) + "=" +
                    WebUtility.UrlEncode(param.Value) + "&";
            }
            requestURL = requestURL.Substring(0, requestURL.Length - 1);

            this.requestURL = requestURL;
            //this.requestURL = "http://localhost:81/samples/bannerMocean.xml";
        }


        private AdRequest(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters,
    AdRequestServed requestServedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback)
        {
            this.userAgent = userAgent;
            this.timeout = timeout;
            this.requestServedCallback = requestServedCallback;
            this.errorCallback = errorCallback;
            this.failedCallback = failedCallback;

            string requestURL = adServerURL + "?";
            foreach (KeyValuePair<String, String> param in parameters)
            {
                requestURL += WebUtility.UrlEncode(param.Key) + "=" +
                    WebUtility.UrlEncode(param.Value) + "&";
            }
            requestURL = requestURL.Substring(0, requestURL.Length - 1);

            this.requestURL = requestURL;
        }

        private AdRequest(int timeout, string adServerURL, string userAgent, Dictionary<String, String> parameters, String postString,
AdRequestServed requestServedCallback, AdRequestError errorCallback, AdRequestFailed failedCallback, bool isNative)
        {
            this.isNative = isNative;
            this.userAgent = userAgent;
            this.timeout = timeout;
            this.postString = postString;

            this.requestServedCallback = requestServedCallback;
            this.errorCallback = errorCallback;
            this.failedCallback = failedCallback;

            string requestURL = adServerURL + "?";
            foreach (KeyValuePair<String, String> param in parameters)
            {
                requestURL += WebUtility.UrlEncode(param.Key) + "=" +
                    WebUtility.UrlEncode(param.Value) + "&";
            }
            requestURL = requestURL.Substring(0, requestURL.Length - 1);

            this.requestURL = requestURL;
        }

        private void SendRequest()
        {
            try
            {
                Task.Factory.StartNew(new Action(TimeoutTask));

                this.webRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(this.requestURL);
                //this.webRequest.Headers[HttpRequestHeader.UserAgent] = this.userAgent;

                IAsyncResult result = this.webRequest.BeginGetResponse(new AsyncCallback(OnReceivedResponse), null);
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }

                Cancel();
            }
        }

        private async void SendPostRequest()
        {
            try
            {
                this.webRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(this.requestURL);
                //this.webRequest.Headers[HttpRequestHeader.UserAgent] = this.userAgent;

                byte[] postData = System.Text.Encoding.UTF8.GetBytes(this.postString);

                this.webRequest.Method = "POST";
                this.webRequest.ContentType = "application/x-www-form-urlencoded";
                //this.webRequest.ContentLength = data.Length;

                Stream newStream = await this.webRequest.GetRequestStreamAsync();
                newStream.Write(postData, 0, postData.Length);
                //newStream.Dispose();

                IAsyncResult result = this.webRequest.BeginGetResponse(new AsyncCallback(OnReceivedResponse), null);
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }

                Cancel();
            }

        }

        private void OnReceivedResponse(IAsyncResult ar)
        {
            try
            {
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)this.webRequest.EndGetResponse(ar);

                if (this.requestServedCallback != null && response.StatusCode == HttpStatusCode.OK)
                {
                    this.requestServedCallback(this, response);
                }
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }
            }
            Cancel();
        }

        public string URL
        {
            get { return this.requestURL; }
        }

        public void Cancel()
        {
            this.completedCallback = null;
            this.errorCallback = null;
            this.failedCallback = null;

            if (timeoutTokenSource != null)
            {
                try
                {
                    timeoutTokenSource.Cancel();
                }
                catch (Exception) { };

                timeoutTokenSource.Dispose();
                timeoutTokenSource = null;
            }

            if (this.webRequest != null)
            {
                this.webRequest.Abort();
                this.webRequest = null;
            }
        }

        private void Start()
        {
            try
            {
                Task.Factory.StartNew(new Action(TimeoutTask));
                this.webRequest = (HttpWebRequest)WebRequest.Create(this.requestURL);
                //this.webRequest.Headers[HttpRequestHeader.UserAgent] = this.userAgent;

                IAsyncResult result = this.webRequest.BeginGetResponse(new AsyncCallback(OnRequestResponse), null);
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }

                Cancel();
            }
        }

        // invoke on request timeout
        private void OnTimeout(Object state)
        {
            if (this.webRequest != null)
            {
                this.webRequest.Abort();
                this.webRequest = null;
            }

            // TODO: Send failure callback with reason
            if (this.failedCallback != null)
            {
                this.failedCallback(this, new TimeoutException());
            }

            Cancel();
        }

        // invoked as timeout task
        private void TimeoutTask()
        {
            timeoutTokenSource = new CancellationTokenSource();
            Task delayTask = Task.Delay(this.timeout, timeoutTokenSource.Token);

            try
            {
                delayTask.Wait();
            }
            catch (Exception) { }

            if (delayTask.Status == TaskStatus.RanToCompletion)
            {
                OnTimeout(null);
            }
        }

        // invoked on response
        private void OnRequestResponse(IAsyncResult ar)
        {
            try
            {
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)this.webRequest.EndGetResponse(ar);

                System.IO.Stream responseStream = response.GetResponseStream();

                Task.Factory.StartNew(new Action<object>(OnParseWork), responseStream);
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }

                Cancel();
            }
        }

        private void OnParseWork(Object state)
        {
            try
            {
                AdDescriptor adDescriptor = null;

                System.IO.Stream stream = state as System.IO.Stream;

                /*  System.IO.StreamReader resourceStreamReader = new System.IO.StreamReader(stream);
                  string mraidScript = resourceStreamReader.ReadToEnd();
                  System.Diagnostics.Debug.WriteLine("Input ad \n" + mraidScript);
                  */
                System.Xml.XmlReaderSettings readerSettings = new System.Xml.XmlReaderSettings();
                readerSettings.IgnoreWhitespace = true;
                readerSettings.IgnoreComments = true;
                readerSettings.IgnoreProcessingInstructions = true;

                System.Xml.XmlReader reader = System.Xml.XmlReader.Create(stream, readerSettings);

                Dictionary<string, string> adInfo = new Dictionary<string, string>();
                Dictionary<string, List<string>> adTrackers = new Dictionary<string, List<string>>();

                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        if (reader.Name == "error")
                        {
                            string errorCode = reader.GetAttribute("code");
                            string errorMessage = string.Empty;

                            // read past the name
                            reader.Read();

                            // read the contents
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.CDATA:
                                case XmlNodeType.Text:
                                    errorMessage = reader.ReadContentAsString();
                                    break;

                            }

                            if (this.errorCallback != null)
                            {
                                this.errorCallback(this, errorCode, errorMessage);

                                Cancel();
                            }

                            // no need to parse anything else
                            break;
                        }
                        else if (reader.Name == "ad")
                        {
                            string adType = reader.GetAttribute("type");

                            adInfo["type"] = adType;

                            // read each child node (passing ad node first)
                            while (reader.Read())
                            {
                                if (!reader.IsStartElement())
                                {
                                    if ((reader.NodeType == System.Xml.XmlNodeType.EndElement) && (reader.Name == "ad"))
                                    {
                                        // done with the descriptor
                                        break;
                                    }

                                    // advance to start of next descriptor property
                                    continue;
                                }

                                string name = reader.Name;

                                if (name.Contains("tracker"))
                                {
                                    List<string> trackers = populateTrackers(reader, name);
                                    if (trackers != null && trackers.Count > 0)
                                        adTrackers[name] = trackers;
                                    continue;
                                }

                                // read past the name
                                reader.Read();

                                // read the content which may be text or cdata from the ad server
                                string value = null;
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.CDATA:
                                    case XmlNodeType.Text:
                                        value = reader.ReadContentAsString();
                                        break;

                                }

                                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                                {
                                    adInfo[name] = value;
                                }
                            }


                            adDescriptor = new AdDescriptor(adInfo, adTrackers);
                            // no need to parse anything else
                            break;
                        }
                    }
                }

                if (this.completedCallback != null)
                {
                    this.completedCallback(this, adDescriptor);
                }
            }
            catch (Exception ex)
            {
                if (this.failedCallback != null)
                {
                    this.failedCallback(this, ex);
                }
            }

            Cancel();
        }
        private List<string> populateTrackers(XmlReader reader, string name)
        {
            List<string> trackers = new List<string>();

            while (reader.Read())
            {
                if (!reader.IsStartElement())
                {
                    if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name.Equals(name)))
                    {
                        // done with the tracker
                        break;
                    }
                    // advance to start of next descriptor property
                    continue;
                }

                reader.Read();
                switch (reader.NodeType)
                {
                    case XmlNodeType.CDATA:
                    case XmlNodeType.Text:
                        string trackUrl = reader.ReadContentAsString();
                        trackers.Add(trackUrl);
                        // Debug.WriteLine("url added " + trackUrl);
                        break;
                }

            }

            return trackers;
        }

        public void Dispose()
        {
            if (this.timeoutTokenSource != null)
            {
                try
                {
                    this.timeoutTokenSource.Cancel();
                }
                catch (Exception) { }

                this.timeoutTokenSource.Dispose();
                this.timeoutTokenSource = null;
            }
        }
    }
}
