﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.Data.Xml.Dom;

namespace VastAd
{
   
    class VastParser
    {
        static public Vast parse(string xml) {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            IXmlNode node = doc.SelectSingleNode("//VAST");
            Vast vast = new Vast(node);
            return vast;
        }
    }
}
