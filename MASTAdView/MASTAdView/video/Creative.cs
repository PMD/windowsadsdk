﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;

namespace VastAd
{
    class Tracking
    {
        public string URI;
        public string ctId;
        public string eventName;

        public string offset;
        private IXmlNode trackingEventNode;

        public Tracking(IXmlNode trackingEventNode)
        {
            this.trackingEventNode = trackingEventNode;

            IXmlNode eventAttr = trackingEventNode.Attributes.GetNamedItem("event");
            if (eventAttr != null)
            {
                this.eventName = eventAttr.NodeValue.ToString();
            }

            IXmlNode offsetAttr = trackingEventNode.Attributes.GetNamedItem("offset");
            if (offsetAttr != null)
            {
                this.offset = offsetAttr.NodeValue.ToString();
            }

            IXmlNode idAttr = trackingEventNode.Attributes.GetNamedItem("id");
            if (idAttr != null)
            {
                this.offset = offsetAttr.NodeValue.ToString();
            }
            this.URI = trackingEventNode.InnerText;

        }
    }
    abstract class Resource
    {
        abstract public bool isRenderable();
    }

    class StaticResource : Resource
    {
        override public bool isRenderable()
        {
            return false;
        }
        public string resourceType;
        public string resourceUrl;
        private IXmlNode staticResourceNode;

        public StaticResource(IXmlNode staticResourceNode)
        {
            this.staticResourceNode = staticResourceNode;

            IXmlNode creativeTypeAttr = staticResourceNode.Attributes.GetNamedItem("creativeType");
            if (creativeTypeAttr != null)
            {
                this.resourceType = creativeTypeAttr.NodeValue.ToString();
            }

            this.resourceUrl = staticResourceNode.InnerText;
        }

        public string getHtml() { return null;  }

    }

    class HTMLResource : Resource
    {
        override public bool isRenderable()
        {
            return false;
        }
        public string value;
        private IXmlNode hTMLResourceNode;

        public HTMLResource(IXmlNode hTMLResourceNode)
        {
            this.hTMLResourceNode = hTMLResourceNode;
        }

        public string getHtml() { return null; }

    }

    class IFrameResource : Resource
    {
        override public bool isRenderable()
        {
            return false;
        }
        public string URL;
        private IXmlNode iFrameResourceNode;

        public IFrameResource(IXmlNode iFrameResourceNode)
        {
            this.iFrameResourceNode = iFrameResourceNode;
        }
    }

    class Creative
    {
        static public Creative creativeForNode(IXmlNode node) {

            Creative creative = null;

            IXmlNode nonLAdsNode = node.SelectSingleNode("NonLinearAds");
            if (nonLAdsNode!=null) {

                creative = new NonLinearAds(nonLAdsNode);
            }

            IXmlNode linAdsNode = node.SelectSingleNode("Linear");
            if (linAdsNode != null)
            {

                creative = new Linear(linAdsNode);
            }

            IXmlNode companionAdsNode = node.SelectSingleNode("CompanionAds");
            if (companionAdsNode != null)
            {

                creative = new CompanionAds(companionAdsNode);
            }
            return creative;
        }

        public  string creativeId;
        public string adId;
        public string sequence;

        public string apiFramework;
    }

    class MediaFile
    {
        //mandatory
        public string delivery;
        public string mediaUrl;
        public string type;
        public float width;
        public float height;

        //optional
        public string codec;
        public string medieFileId;
        public int bitrate;
        public int minBitrate;
        public int maxBitrate;

        public bool scalable;
        public bool maintainAspectratio;
        public string apiFramework;
        private IXmlNode node;

        public MediaFile(IXmlNode node)
        {
            /*< MediaFile maintainAspectRatio = "true" scalable = "true" height = "300" width = "400" bitrate = "500" type = "video/x-flv" delivery = "progressive" > http://cdnp.tremormedia.com/video/acudeo/Carrot_400x300_500kb.flv</MediaFile>

</ MediaFiles >
*/
            this.node = node;
            IXmlNode heightAttr = node.Attributes.GetNamedItem("height");
            if (heightAttr != null)
            {
                this.height = int.Parse(heightAttr.NodeValue.ToString());
            }
            IXmlNode widthtAttr = node.Attributes.GetNamedItem("width");
            if (widthtAttr != null)
            {
                this.width = int.Parse(widthtAttr.NodeValue.ToString());
            }
            IXmlNode typeAttr = node.Attributes.GetNamedItem("type");
            if (typeAttr != null)
            {
                this.type = typeAttr.NodeValue.ToString();
            }

            //this.mediaUrl = "http://rmcdn.2mdn.net/MotifFiles/html/1248596/android_1330378998288.mp4";// node.InnerText;
            this.mediaUrl = node.InnerText;
        }
    }
    class NonLinear
    {
        public List<Resource> resources;
        public float width;
        public float height;

        public string catId;
        public string apiFramework;
        public float expandedWidth;
        public float expandedHeight;
        public bool scalable;
        public bool maintainAspectratio;

        public string minSuggestedDuration;

        public Array creativeExtensions;
        public Array adParameters;
        public List<String> nonLinearClickTrackers;
        public string nonLinearClickThrough;
        private IXmlNode nonLinearNode;

        public NonLinear(IXmlNode nonLinearNode)
        {
            this.nonLinearNode = nonLinearNode;
            IXmlNode heightAttr = nonLinearNode.Attributes.GetNamedItem("height");
            if (heightAttr != null)
            {
                this.height = int.Parse(heightAttr.NodeValue.ToString());
            }
            IXmlNode widthtAttr = nonLinearNode.Attributes.GetNamedItem("width");
            if (widthtAttr != null)
            {
                this.width = int.Parse(widthtAttr.NodeValue.ToString());
            }

            IXmlNode minSuggestedDurationAttr = nonLinearNode.Attributes.GetNamedItem("minSuggestedDuration");
            if (minSuggestedDurationAttr != null)
            {
                this.minSuggestedDuration = minSuggestedDurationAttr.NodeValue.ToString();
            }

            IXmlNode nonLinearClickThroughNode = nonLinearNode.SelectSingleNode("//NonLinearClickThrough");
            if (nonLinearClickThroughNode!=null)
            {
                this.nonLinearClickThrough = nonLinearClickThroughNode.InnerText;
            }

            XmlNodeList nonLinearClickTrackingNodes = nonLinearNode.SelectNodes("//NonLinearClickTracking");
            this.nonLinearClickTrackers = new List<string>();
            foreach (IXmlNode clickTracking in nonLinearClickTrackingNodes)
            {

                this.nonLinearClickTrackers.Add(clickTracking.InnerText);
            }

            resources = new List<Resource>();

            XmlNodeList staticRersourceNodes = nonLinearNode.SelectNodes("//StaticResource");
            foreach (IXmlNode staticResourceNode in staticRersourceNodes)
            {
                StaticResource resource = new StaticResource(staticResourceNode);
                this.resources.Add(resource);
            }

            XmlNodeList iFrameResourceNodes = nonLinearNode.SelectNodes("//IFrameResource");
            foreach (IXmlNode iFrameResourceNode in iFrameResourceNodes)
            {
                IFrameResource resource = new IFrameResource(iFrameResourceNode);
                this.resources.Add(resource);
            }

            XmlNodeList HTMLResourceNodes = nonLinearNode.SelectNodes("//HTMLResource");
            foreach (IXmlNode HTMLResourceNode in HTMLResourceNodes)
            {
                HTMLResource resource = new HTMLResource(HTMLResourceNode);
                this.resources.Add(resource);
            }
        }
    }

    class Linear:Creative
    {
        public string duration;
        public List<MediaFile> mediaFiles;

        public string videoClickThroughURI;

        public List<String> videoClickTrackingURIs;
        public Array adParams;

        public List<Tracking> trackingEvents;

        public Array icons;
        public string skipOffset;
        private IXmlNode linAdsNode;

        public Linear(IXmlNode linAdsNode)
        {
            this.mediaFiles = new List<MediaFile>();
            this.linAdsNode = linAdsNode;
            XmlNodeList list = this.linAdsNode.SelectNodes("//MediaFiles/MediaFile");

            foreach (IXmlNode node in list)
            {
                MediaFile mediaFile = new MediaFile(node);
                this.mediaFiles.Add(mediaFile);
            }

            IXmlNode durationNode = linAdsNode.SelectSingleNode("//Duration");
            if (durationNode != null)
            {
                this.duration = durationNode.InnerText;
            }
            
            XmlNodeList trackingEevntNodes = this.linAdsNode.SelectNodes("//TrackingEvents/Tracking");

            this.trackingEvents = new List<Tracking>();
            foreach (IXmlNode trackingEventNode in trackingEevntNodes)
            {
                Tracking trackingEvent = new Tracking(trackingEventNode);
                this.trackingEvents.Add(trackingEvent);
            }

            IXmlNode clickThruNode = linAdsNode.SelectSingleNode("//VideoClicks/ClickThrough");
            if (clickThruNode != null)
            {
                this.videoClickThroughURI = clickThruNode.InnerText;
            }

            XmlNodeList clickTrackerNodes = linAdsNode.SelectNodes("//VideoClicks/ClickTracking");

            this.videoClickTrackingURIs = new List<string>();
            foreach (IXmlNode clickTracking in clickTrackerNodes)
            {

                this.videoClickTrackingURIs.Add(clickTracking.InnerText);
            }
       

        }
    }

    class NonLinearAds : Creative
    {
        public List<NonLinear> nonLinearAds;

        public List<Tracking> trackingEvents;
        private IXmlNode nonLAdsNode;

        public NonLinearAds(IXmlNode nonLAdsNode)
        {
            this.nonLAdsNode = nonLAdsNode;
            XmlNodeList trackingEevntNodes = this.nonLAdsNode.SelectNodes("//TrackingEvents/Tracking");

            this.trackingEvents = new List<Tracking>();
            foreach (IXmlNode trackingEventNode in trackingEevntNodes)
            {
                Tracking trackingEvent = new Tracking(trackingEventNode);
                this.trackingEvents.Add(trackingEvent);
            }

            XmlNodeList nonLinearList = this.nonLAdsNode.SelectNodes("//NonLinear");
            this.nonLinearAds = new List<NonLinear>();

            foreach (IXmlNode nonLinearNode in nonLinearList)
            {
                NonLinear nonLinear = new NonLinear(nonLinearNode);
                this.nonLinearAds.Add(nonLinear);
            }
        }


    }

    class CompanionAds : Creative
    {
        public List<Companion> companionAds;

        public string required;
        private IXmlNode companionAdsNode;

        public CompanionAds(IXmlNode companionAdsNode)
        {
            this.companionAdsNode = companionAdsNode;

            XmlNodeList companionNodes = this.companionAdsNode.SelectNodes("//Companion");
            this.companionAds = new List<Companion>();

            foreach (IXmlNode companion in companionNodes)
            {
                Companion nonLinear = new Companion(companion);
                this.companionAds.Add(nonLinear);
            }
        }

      
    }

    class Companion
    {
        public float width;
        public float height;

        public List<Resource> resources;
        public List<String> companionClickTrackers;
        public string companionClickThrough;

        public Companion(IXmlNode companionNode)
        {
            IXmlNode heightAttr = companionNode.Attributes.GetNamedItem("height");
            if (heightAttr != null)
            {
                this.height = int.Parse(heightAttr.NodeValue.ToString());
            }
            IXmlNode widthtAttr = companionNode.Attributes.GetNamedItem("width");
            if (widthtAttr != null)
            {
                this.width = int.Parse(widthtAttr.NodeValue.ToString());
            }

            IXmlNode companionClickThroughNode = companionNode.SelectSingleNode("//CompanionClickThrough");
            if (companionClickThroughNode != null)
            {
                this.companionClickThrough = companionClickThroughNode.InnerText;
            }

            XmlNodeList comapanionClickTrackingNodes = companionNode.SelectNodes("//CompanionClickTracking");
            this.companionClickTrackers = new List<string>();
            foreach (IXmlNode companionTracking in comapanionClickTrackingNodes)
            {

                this.companionClickTrackers.Add(companionTracking.InnerText);
            }

            this.resources = new List<Resource>();

            XmlNodeList staticRersourceNodes = companionNode.SelectNodes("//StaticResource");
            foreach (IXmlNode staticResourceNode in staticRersourceNodes)
            {
                StaticResource resource = new StaticResource(staticResourceNode);
                this.resources.Add(resource);
            }

            XmlNodeList iFrameResourceNodes = companionNode.SelectNodes("//IFrameResource");
            foreach (IXmlNode iFrameResourceNode in iFrameResourceNodes)
            {
                IFrameResource resource = new IFrameResource(iFrameResourceNode);
                this.resources.Add(resource);
            }

            XmlNodeList HTMLResourceNodes = companionNode.SelectNodes("//HTMLResource");
            foreach (IXmlNode HTMLResourceNode in HTMLResourceNodes)
            {
                HTMLResource resource = new HTMLResource(HTMLResourceNode);
                this.resources.Add(resource);
            }
        }
    }
}


