﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;


namespace VastAd
{

    enum VastAdType { VastAdTypeNone, VastAdTypeInLine, VastAdTypeWrapper };
    public class AdSystem
    {
        private IXmlNode adSystemNode;

        public AdSystem(IXmlNode adSystemNode)
        {
            this.adSystemNode = adSystemNode;
        }
    }
    public class Pricing
    {
    }
    class Ad
    {

        public override string ToString()
        {
            return "\nID : "+adId+"\nsequence"+sequence+"\nAd title "+adTitle+"\n Desc "+adDescription+"IMp cound"+impressionTrackers.Count();
        }
        public Ad(IXmlNode node) {

            IXmlNode idNode = node.Attributes.GetNamedItem("id");
            IXmlNode sequenceNode = node.Attributes.GetNamedItem("sequence");
            if (idNode != null)
            {
                this.adId = idNode.NodeValue.ToString();

            }
            if (sequenceNode != null)
            {
                this.sequence = sequenceNode.NodeValue.ToString();
            }

            IXmlNode inLine = node.SelectSingleNode("InLine");
            if (inLine!=null)
            {
                this.adType = VastAdType.VastAdTypeInLine;
                handleInLine(inLine);
            }

            IXmlNode wrapper = node.SelectSingleNode("Wrapper");
            if (wrapper != null)
            {
                this.adType = VastAdType.VastAdTypeWrapper;
                handleWrapper(wrapper);
            }

        }

        private void handleWrapper(IXmlNode wrapper)
        {
            throw new NotImplementedException();
        }

        private void handleInLine(IXmlNode inLine)
        {
            IXmlNode adSystemNode = inLine.SelectSingleNode("//AdSystem");
            AdSystem adSystem = new AdSystem(adSystemNode);
            this.adSystem = adSystem;
            IXmlNode adTitleNode = inLine.SelectSingleNode("//AdTitle");
            this.adTitle = adTitleNode.InnerText;
            IXmlNode adDescriptionNode = inLine.SelectSingleNode("//Description");
            this.adDescription = adDescriptionNode.InnerText;
            IXmlNode adSurveyNode = inLine.SelectSingleNode("//Survey");
            if (adSurveyNode != null)
            {
                this.surveryURI = adSurveyNode.InnerText;
            }
            processImpressions(inLine);
            processCreatives(inLine);

        }

        private void processCreatives(IXmlNode inLine)
        {
            this.creatives =new List<Creative>();
            XmlNodeList list = inLine.SelectNodes("//Creatives/Creative");
            foreach (IXmlNode node in list)
            {
                Creative creative = Creative.creativeForNode(node);
                this.creatives.Add(creative);
                System.Diagnostics.Debug.WriteLine("\n\n"+node.NodeName.ToString());

            }
        }

        private void processImpressions(IXmlNode inLine)
        {
            this.impressionTrackers = new List<string>();
           XmlNodeList list =  inLine.SelectNodes("//Impression");
            foreach(IXmlNode node in list)
            {
                this.impressionTrackers.Add( node.InnerText);
            }
        }

        public Ad()
        {
        }

        public string sequence;
        public string adId;

        public AdSystem adSystem;
        public string adTitle;
        public string adDescription;
        public string advertiser;

        public Pricing pricing;
        public VastAdType adType;

        public String surveryURI;
        public List<string> errorTracker;
        public List<string> impressionTrackers;
        public List<Creative> creatives;
        public List<string> extensions;

        public string adTagURI;




    }
    class Vast
    {
        override public string ToString() {

            string desc = "\nVersion" + version + "\nAd count " + this.ads.Count();
            Ad ad = this.ads[0];
            desc = desc + ad.ToString();
            return desc;
        }

        public string version;
        public List<Ad>ads = null;
        public Array wrapperAds = null;

        public Vast(IXmlNode node)
        {

            string version = node.Attributes.GetNamedItem("version").NodeValue.ToString();
            this.version = version;

            IXmlNode firstChild = node.SelectSingleNode("//Ad");
            if (firstChild.LocalName.ToString().CompareTo("Ad") == 0) {

                Ad ad = new Ad(firstChild);
                this.ads = new List<Ad>();
                this.ads.Add(ad);

            }


        }

        public Ad currentAd() {

            Ad ad = null;

            if (this.ads.Count > 0)
            {
                ad = this.ads[0];
            }
            return ad;
        }


    }


}
