﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using VastAd;

namespace MASTAdView.video
{

    class VastRequestHandler
    {

        /// <summary>
        /// Event triggered when a logging event occurs.
        /// </summary>
        public event LoggingEventEventHandler LoggingEvent;

        public delegate void VastResponseRecieved(Vast vast);

        public delegate void VastRequestFailed(int errorType, int errorCode);

        /// <summary>
        /// Delegate for the LoggingEvent event.
        /// Note: The delegate may be invoked on a non-UI thread.
        /// </summary>
        /// <param name="sender">Instance invoking the delegate.</param>
        /// <param name="e">Event args providing event information.</param>
        public delegate void LoggingEventEventHandler(object sender, LoggingEventEventArgs e);

        private AdRequest adRequest = null;

        private VastResponseRecieved vastSuccessDelegate;

        private VastRequestFailed vastFailureDelegate;

        public VastRequestHandler(VastResponseRecieved vastSuccessDelegate, VastRequestFailed vastFailureDelegate)
        {
            this.vastSuccessDelegate = vastSuccessDelegate;
            this.vastFailureDelegate = vastFailureDelegate;
        }

        public void requestAd(string requestUrl, Dictionary<string, string> args)
        {

            this.adRequest = AdRequest.InitiateRequest(30000, requestUrl, "", args,
                new AdRequestServed(adRequestCompleted),
                new AdRequestError(adRequestError),
                new AdRequestFailed(adRequestFailed));

        }


        private void adRequestFailed(AdRequest request, Exception exception)
        {
            LogEvent(LogLevel.Error, exception.Message);
        }

        private void adRequestError(AdRequest request, string errorCode, string errorMessage)
        {
            string message = string.Format("Error response from server.  Error code:{0} - Error message:{1} - Request:{2}",
                errorCode, errorMessage, request.URL);

            LogLevel level = LogLevel.Error;
            if (errorCode == "404")
                level = LogLevel.Debug;

            LogEvent(level, message);
        }

        private void adRequestCompleted(AdRequest adRequest, System.Net.HttpWebResponse response)
        {
            if (adRequest != this.adRequest)
                return;

            if (response == null)
            {
                string message = "No ad available in response.";
                LogEvent(LogLevel.Error, message);
                return;
            }

            //------------------- Parsing starts ------------------
            try
            {
                Stream stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(stream);

                string respStr = reader.ReadToEnd();

                System.Diagnostics.Debug.WriteLine("respo str - " + respStr.ToString());

                Vast vast = VastParser.parse(respStr);

                System.Diagnostics.Debug.WriteLine("==> Parsed VAST :: " +vast.ToString());

                this.vastSuccessDelegate(vast);
            } catch(Exception ex)
            {
                Debug.WriteLine("VAST Parsing exception :: "+ ex.ToString());
            }
            //--------------- Parsing ends --------------

        }

        private bool OnLoggingEvent(LogLevel logLevel, string entry)
        {
            LoggingEventEventArgs args = new LoggingEventEventArgs(logLevel, entry);

            if (this.LoggingEvent != null)
            {
                this.LoggingEvent(this, args);
            }

            return args.Cancel == false;
        }
        #region Logging

        /// <summary>
        /// LoggingEvent EventArgs
        /// Note:  The SDK logging of the event can be canceled through the Cancel property.
        /// If canceled, the application is responsible for logging the event as necessary.
        /// </summary>
        public class LoggingEventEventArgs : CancelEventArgs
        {
            public LoggingEventEventArgs(LogLevel logLevel, string entry)
            {
                this.logLevel = logLevel;
                this.entry = entry;
            }

            private readonly LogLevel logLevel;
            /// <summary>
            /// The LogLevel of the event.
            /// </summary>
            public object LogLevel
            {
                get { return this.logLevel; }
            }

            private readonly string entry;
            /// <summary>
            /// The logging event entry.
            /// </summary>
            public string Entry
            {
                get { return this.entry; }
            }
        }

        private LogLevel logLevel = LogLevel.Error;
        /// <summary>
        /// Sets the log level of the SDK.  Logging is done through the system console only.
        /// Developers desiring to record log data to a custom log file can be done through the LoggingEvent event.
        /// By default the SDK is set to LogLevel.Error.
        /// Production releases should not be set to LogLevel.Debug.
        /// </summary>
        public LogLevel LogLevel
        {
            get { return this.logLevel; }
            set { this.logLevel = value; }
        }

        private void LogEvent(LogLevel level, string entry)
        {
            if (level > this.LogLevel)
                return;

            bool shouldLog = OnLoggingEvent(level, entry);

            if (shouldLog == false)
                return;

            string line = string.Format("MASTAdView:{0}\n\tType:{1}\n\tEvent:{2}", this, level.ToString(), entry);
            Debug.WriteLine(line);
        }

        #endregion

    }
}
