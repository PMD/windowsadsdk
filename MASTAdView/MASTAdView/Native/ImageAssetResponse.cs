﻿namespace MASTNativeAdView
{
    public class ImageAssetResponse : AssetResponse
    {
        /** Image Asset */
        public MASTNativeAd.Image Image;

        /** Image Type (Optional in response) */
        public ImageAssetTypes ImageType;

        public MASTNativeAd.Image GetImage()
        {
            return Image;
        }

        public void SetImage(MASTNativeAd.Image image)
        {
            this.Image = image;
        }

        /**
         * Image type is only available in case of mediation response
         * 
         * @return Type of image asset
         */
        public ImageAssetTypes GetImageType()
        {
            return ImageType;
        }

        public void SetImageType(ImageAssetTypes imageType)
        {
            this.ImageType = imageType;
        }

        public int GetTypeId()
        {
            if (ImageType == ImageAssetTypes.Icon)
                return 1;
            else if (ImageType == ImageAssetTypes.Logo)
                return 2;
            else if (ImageType == ImageAssetTypes.Main)
                return 3;
            else
                return -1;
        }
    }
}
