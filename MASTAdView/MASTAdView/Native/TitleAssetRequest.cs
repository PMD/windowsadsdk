﻿namespace MASTNativeAdView
{
    public class TitleAssetRequest : AssetRequest
    {
        /** Character length of title asset */
        public int Length;

        public int GetLength()
        {
            return Length;
        }

        public void SetLength(int length)
        {
            this.Length = length;
        }
    }
}
