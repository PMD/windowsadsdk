﻿using MASTAdView;
using System;
using System.Collections.Generic;

namespace MASTNativeAdView
{
    public class NativeAdDescriptor : AdDescriptor
    {
        private String CreativeId = null;
        private String FeedId = null;
        private String Click = null;
        private String[] NativeImpressionTrackers = null;
        private String JsTracker = null;
        private String[] NativeClickTrackers = null;
        private String NativeAdJSON = null;
        private int NativeVersion = 0;
        private String Subtype = "";
        private String Mediation = null; // mediation partner name
        private String MediationId = null; // id of the mediation partner
        private String FallbackUrl = null;
        private List<AssetResponse> NativeAssetList = null;

        /**
         * Denotes whether the response received is of mediation or native
         */
        private bool TypeMediation = false;

        /**
         * Source of the advertisement, can be direct or mediation.
         */
        private String Source = null;

        private String AdUnitId = null;

        /**
         * @param subtype
         * @param clickUrl
         * @param fallbackUrl
         * @param impressionTrackers
         * @param clickTrackers
         * @param jsTrackerStringArray
         * @param nativeAssetList
         */
        public NativeAdDescriptor(String subtype, int nativeVersion2, String clickUrl, String fallbackUrl,
        String[] impressionTrackers, String[] clickTrackers, String jsTrackerString,
        List<AssetResponse> nativeAssetList)
        {
            this.Subtype = subtype;
            this.Click = clickUrl;
            this.NativeImpressionTrackers = impressionTrackers;
            this.NativeClickTrackers = clickTrackers;
            this.JsTracker = jsTrackerString;
            this.FallbackUrl = fallbackUrl;
            this.NativeAssetList = nativeAssetList;
            this.NativeVersion = nativeVersion2;
            this.TypeMediation = false;
        }

        /**
         * @param subtype
         * @param creativeId
         * @param mediation
         * @param mediation
         * @param mediationId
         * @param adUnitId
         * @param impressionTrackers
         * @param clickTrackers
         * @param jsTrackerStringArray
         * @param feedId
         */
        public NativeAdDescriptor(String subtype, String creativeId, String mediation, String mediationId, String adUnitId,
                String source, String[] impressionTrackers, String[] clickTrackers, String jsTrackerString, String feedId) : base()
        {
            this.Subtype = subtype;
            this.CreativeId = creativeId;
            this.Mediation = mediation;
            this.MediationId = mediationId;
            this.AdUnitId = adUnitId;
            this.Source = source;
            this.NativeImpressionTrackers = impressionTrackers;
            this.NativeClickTrackers = clickTrackers;
            this.JsTracker = jsTrackerString;
            this.FeedId = feedId;
            this.TypeMediation = true;
        }

        /**
         * Returns the click url
         * 
         * @return the click
         */
        public String GetClick()
        {
            return Click;
        }

        /**
         * Set the click url
         * 
         * @param click
         */
        public void SetClick(String click)
        {
            this.Click = click;
        }

        /**
         * Set the fallback Url
         * 
         * @param fallback
         */
        public void SetFallbackUrl(String fallbackUrl)
        {
            this.FallbackUrl = fallbackUrl;
        }

        /**
         * @return the fallbackUrl to be used if click url deep-link does not work
         *         on device
         */
        public String GetFallbackUrl()
        {
            return FallbackUrl;
        }

        /**
         * @return the impressionTrackers
         */
        public String[] GetNativeAdImpressionTrackers()
        {
            return NativeImpressionTrackers;
        }

        /**
         * @return the clickTrackers
         */
        public String[] GetNativeAdClickTrackers()
        {
            return NativeClickTrackers;
        }

        /**
         * @return the jsTracker received in native json response.
         */
        public String GetJsTracker()
        {
            return JsTracker;
        }

        /**
         * Setter for jsTracer
         * 
         * @param jsTracker
         */
        public void SetJsTracker(String jsTracker)
        {
            this.JsTracker = jsTracker;
        }

        /**
         * @return the nativeAdJSON
         */
        public String GetNativeAdJSON()
        {
            return NativeAdJSON;
        }

        /**
         * 
         * @param nativeAdJSON
         */
        public void SetNativeAdJSON(String nativeAdJSON)
        {
            this.NativeAdJSON = nativeAdJSON;
        }

        /**
         * @return the Subtype
         */
        String GetSubtype()
        {
            return Subtype;
        }

        /**
         * @param subtype
         *            the subtype to set
         */
        void SetSubtype(String subtype)
        {
            this.Subtype = subtype;
        }

        /**
         * @return the creativeId
         */
        String GetCreativeId()
        {
            return CreativeId;
        }

        /**
         * @param creativeId
         *            the creativeId to set
         */
        void SetCreativeId(String creativeId)
        {
            this.CreativeId = creativeId;
        }

        /**
         * @param feedId
         *            Ad feed partner identifier in Mocean
         */
        public void SetFeedId(String feedId)
        {
            this.FeedId = feedId;
        }

        /**
         * 
         * @return feedId Ad feed partner identifier in Mocean
         */
        public String GetFeedId()
        {
            return FeedId;
        }

        /**
         * @return the mediation
         */
        String GetMediation()
        {
            return Mediation;
        }

        /**
         * @param mediation
         *            the mediation to set
         */
        void SetMediation(String mediation)
        {
            this.Mediation = mediation;
        }

        /**
         * @return the mediationId
         */
        String GetMediationId()
        {
            return MediationId;
        }

        /**
         * @param mediationId
         *            the feedId to set
         */
        void SetMediationId(String mediationId)
        {
            this.MediationId = mediationId;
        }

        /**
         * @return the source
         */
        String GetSource()
        {
            return Source;
        }

        /**
         * @param source
         *            the source to set
         */
        void SetSource(String source)
        {
            this.Source = source;
        }

        /**
         * 
         * @return AdUnitId
         */
        String GetAdUnitId()
        {
            return AdUnitId;
        }

        /**
         * 
         * @param adUnitId
         */
        void SetMediationData(String adUnitId)
        {
            this.AdUnitId = adUnitId;
        }

        /**
         * Denotes whether response received is of type mediation or native.
         * 
         * @return
         */
        bool IsTypeMediation()
        {
            return TypeMediation;
        }

        public List<AssetResponse> GetNativeAssetList()
        {
            return NativeAssetList;
        }

        public void SetNativeAssetList(List<AssetResponse> nativeAssetList)
        {
            this.NativeAssetList = nativeAssetList;
        }

        public int GetNativeVersion()
        {
            return NativeVersion;
        }

        public void SetNativeVersion(int nativeVersion)
        {
            this.NativeVersion = nativeVersion;
        }
    }
}
