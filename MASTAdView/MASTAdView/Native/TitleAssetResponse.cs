﻿using System;

namespace MASTNativeAdView
{
    public class TitleAssetResponse : AssetResponse
    {
        /** Title text received */
        private String TitleText;

        public String GetTitleText()
        {
            return TitleText;
        }

        public void SetTitleText(String titleText)
        {
            this.TitleText = titleText;
        }
    }
}
