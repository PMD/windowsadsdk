﻿namespace MASTNativeAdView
{
    public class DataAssetRequest : AssetRequest
    {
        /** Length for DataAsset */
        public int Length;

        /** Data Asset Type */
        public DataAssetTypes DataAssetType;

        public int GetLength()
        {
            return Length;
        }

        public void SetLength(int length)
        {
            this.Length = length;
        }

        public DataAssetTypes GetDataAssetType()
        {
            return DataAssetType;
        }

        public void SetDataAssetType(DataAssetTypes dataAssetType)
        {
            this.DataAssetType = dataAssetType;
        }

        public int GetTypeId()
        {
            if (DataAssetType == DataAssetTypes.sponsored)
                return 1;
            else if (DataAssetType == DataAssetTypes.desc)
                return 2;
            else if (DataAssetType == DataAssetTypes.rating)
                return 3;
            else if (DataAssetType == DataAssetTypes.likes)
                return 4;
            else if (DataAssetType == DataAssetTypes.downloads)
                return 5;
            else if (DataAssetType == DataAssetTypes.price)
                return 6;
            else if (DataAssetType == DataAssetTypes.saleprice)
                return 7;
            else if (DataAssetType == DataAssetTypes.phone)
                return 8;
            else if (DataAssetType == DataAssetTypes.address)
                return 9;
            else if (DataAssetType == DataAssetTypes.desc2)
                return 10;
            else if (DataAssetType == DataAssetTypes.displayurl)
                return 11;
            else if (DataAssetType == DataAssetTypes.ctatext)
                return 12;
            else if (DataAssetType == DataAssetTypes.OTHER)
                return 501;
            else
                return -1;
        }
    }
}
