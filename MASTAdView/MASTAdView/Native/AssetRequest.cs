﻿namespace MASTNativeAdView
{
    public class AssetRequest
    {
        /** Asset Id */
        public int AssetId;

        /** Flag to check if asset is required or optional */
        public bool IsAssetRequired = false;

        public int GetAssetId()
        {
            return AssetId;
        }

        public void SetAssetId(int assetId)
        {
            this.AssetId = assetId;
        }

        public bool IsRequired()
        {
            return IsAssetRequired;
        }

        /**
         * Set if asset is required or optional.
         * 
         * Default: false (optional)
         * 
         * @param isRequired
         *            : Set true for required or false for optional.
         */
        public void SetRequired(bool isRequired)
        {
            this.IsAssetRequired = isRequired;
        }
    }
}
