﻿using System;

namespace MASTNativeAdView
{
    public class DataAssetResponse : AssetResponse
    {
        /** Data Asset Type */
        public DataAssetTypes DataAssetType;

        /** Value of data asset received */
        public String Value;

        public DataAssetTypes GetDataAssetType()
        {
            return DataAssetType;
        }

        public void SetDataAssetType(DataAssetTypes dataAssetType)
        {
            this.DataAssetType = dataAssetType;
        }

        public String GetValue()
        {
            return Value;
        }

        public void SetValue(String value)
        {
            this.Value = value;
        }
    }
}
