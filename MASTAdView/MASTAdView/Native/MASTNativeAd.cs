﻿using MASTAdView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.Networking.Connectivity;

namespace MASTNativeAdView
{
    public class MASTNativeAd
    {
        public enum LogLevel
        {
            None, Error, Debug
        }

        private enum CallbackType
        {
            NativeReceived, NativeFailed, ThirdPartyReceived, NativeAdClicked
        }

        /**
         * Invoked when the SDK logs events. If applications override logging
         * they can return true to indicate the log event has been consumed and
         * the SDK processing is not needed.
         * <p>
         * Will not be invoked if the MASTNativeAd instance's logLevel is set
         * lower than the event.
         * 
         * @param nativeAd
         * @param event
         *            String representing the event to be logged.
         * @param eventLevel
         *            LogLevel of the event.
         * @return
         */
        public interface LogListener
        {
            bool onLogEvent(MASTNativeAd nativeAd, string event1, LogLevel eventLevel);
        }

        public interface INativeRequestListener
        {
            /**
             * Callback when the native ad is received.
             * 
             * @param ad
             */
            void OnNativeAdReceived(MASTNativeAd ad);

            /**
             * Callback when MASTNativeAd fails to get an ad. This may be due to
             * invalid zone, network connection not available, timeout, no Ad to
             * fill at this point.
             * 
             * @param ad
             *            - MASTNativeAd object which has requested for the Ad.
             * @param ex
             *            - Exception occurred.
             */
            void OnNativeAdFailed(MASTNativeAd ad, Exception ex);

            /**
             * Third party ad received. The application should be expecting this and
             * ready to render the ad with the supplied configuration.
             * 
             * @param ad
             * @param properties
             *            Properties of the ad request (ad network information).
             * @param parameters
             *            Parameters for the third party network (expected to be
             *            passed to that network).
             */
            void OnReceivedThirdPartyRequest(MASTNativeAd ad,
                   Dictionary<String, String> properties, Dictionary<String, String> parameters);

            /**
             * Callback when the user clicks on the native ad. Implement your logic
             * on view click in this method.
             * 
             * <B> Do not implement your own clickListener in your activity. </b>
             * 
             * @param ad
             */
            void OnNativeAdClicked(MASTNativeAd ad);
        }

        private Page Context;
        private int Zone = 0;

        // Used to store the the current native response
        private NativeAdDescriptor NativeAdDescriptor = null;
        private bool ImpressionTrackerSent = false;
        private bool ThirdPartyImpTrackerSent = false;
        private bool ThirdPartyClickTrackerSent = false;
        private bool ClickTrackerSent = false;
        private INativeRequestListener RequestListener = null;
        private String UserAgent = null;
        private bool UserAgentResume = false;
        private LogListener mLogListener = null;
        private LogLevel Level = LogLevel.None;
        private bool Test = false;
        private Dictionary<String, String> AdRequestParameters;
        private List<AssetRequest> RequestedNativeAssets;

        // Use external system native browser by default
        private bool UseInternalBrowser = false;

        /**
        * If false, denotes that SDK should handle the third party request with the
        * help of adapter. If true, SDK should give the control back to publisher
        * to handle third party request.
        */
        private bool OverrideAdapter = true;
        private String NetworkUrl = Defaults.AD_NETWORK_URL;

        private AdRequest mAdRequest = null;

        /**
         * @param context
         */
        public MASTNativeAd(Page context)
        {
            Context = context;
            AdRequestParameters = new Dictionary<String, String>();
            RequestedNativeAssets = new List<AssetRequest>();
        }

        /**
         * Sets the zone on the ad network to obtain ad content.
         * 
         * @param zone
         *            Ad network zone.
         */
        public void SetZone(int zone)
        {
            this.Zone = zone;
        }

        /**
         * Returns the currently configured ad network zone.
         * 
         * @return Ad network zone.
         */
        public int GetZone()
        {
            return Zone;
        }

        /**
         * Sets the instance test mode. If set to test mode the instance will
         * request test ads for the configured zone.
         * <p>
         * Warning: This should never be enabled for application releases.
         * 
         * @param test
         *            true to set test mode, false to disable test mode.
         */
        public void SetTest(bool test)
        {
            this.Test = test;
        }

        /**
         * Access for test mode state of the instance.
         * 
         * @return true if the instance is set to test mode, false if test mode is
         *         disabled.
         */
        public bool IsTest()
        {
            return Test;
        }

        public void SetRequestListener(INativeRequestListener requestListener)
        {
            if (requestListener == null
                    || !(requestListener is INativeRequestListener))
            {
                throw new ArgumentException("Kindly pass the object of type NativeRequestListener in case you want to use NativeAdView."
                                + " Implement NativeRequestListener in your activity instead of RequestListener. ");
            }
            else
            {
                RequestListener = requestListener;
            }
        }

        /**
         * Sets the log listener. This listener provides the ability to override
         * default logging behavior.
         * 
         * @param logListener
         *            MASTNativeAd.LogListener implementation
         */
        public void SetLogListener(LogListener logListener)
        {
            this.mLogListener = logListener;
        }

        /**
         * Returns the currently configured listener.
         * 
         * @return MASTNativeAd.LogListener set with setLogListener().
         */
        public LogListener GetLogListener()
        {
            return mLogListener;
        }

        /**
         * Sets the log level of the instance. Logging is done through console
         * logging.
         * 
         * @param logLevel
         *            LogLevel
         */
        public void SetLogLevel(LogLevel logLevel)
        {
            this.Level = logLevel;
        }

        /**
         * Returns the currently configured log level.
         * 
         * @return currently configured LogLevel
         */
        public LogLevel GetLogLevel()
        {
            return Level;
        }

        /**
         * List of requested native assets.
         * <p>
         * Returns subclasses of {@link AssetRequest} class viz:
         * {@link TitleAssetRequest}, {@link ImageAssetRequest} and
         * {@link DataAssetRequest}.
         * <p>
         * Sub-type of asset can be identified by 'type' variable where ever
         * available.
         * 
         * @return
         */
        public List<AssetRequest> GetRequestedNativeAssetsList()
        {
            return RequestedNativeAssets;
        }

        /**
         * Add native asset request in ad request.
         * <p>
         * <b>NOTE:</b> Use unique assetId for each asset in asset request. The same
         * assetId will be returned in native ad response. You can use this assetId
         * to map requested native assets with response.
         * 
         * @see addNativeAssetRequestList(List<AssetRequest> assetList) :
         *      Convenience method to add all assets at once.
         * @param asset
         *            Native {@link AssetRequest} to add in the ad request
         */
        public void AddNativeAssetRequest(AssetRequest asset)
        {
            if (RequestedNativeAssets != null && asset != null)
            {
                RequestedNativeAssets.Add(asset);
            }
        }

        /**
        * Convenience method to add all native asset requests at once. *
        * <p>
        * <b>NOTE:</b> Use unique assetId for each asset in asset request. The same
        * assetId will be returned in native ad response. You can use this assetId
        * to map requested native assets with response.
        * 
        * @see addNativeAssetRequest(AssetRequest asset)
        * @param assetList
        *            List of Native {@link AssetRequest} to add in the ad request
        */
        public void AddNativeAssetRequestList(List<AssetRequest> assetList)
        {
            if (RequestedNativeAssets != null && assetList != null)
            {
                // Put all the user sent parameters in the request
                foreach (var asset in assetList)
                    RequestedNativeAssets.Add(asset);
            }
        }

        /**
         * Collection of ad request parameters. Allows setting extra network
         * parameters.
         * <p>
         * The SDK will set various parameters based on configuration and other
         * options. For more information see
         * http://developer.moceanmobile.com/Mocean_Ad_Request_API.
         * 
         * @return Map containing optional request parameters.
         */
        public Dictionary<String, String> GetAdRequestParameters()
        {
            return AdRequestParameters;
        }

        /**
         * Allows setting of extra custom parameters to ad request. Add custom
         * parameter (key-value).
         * 
         * @see - For details of custom parameters refer
         *      http://developer.moceanmobile.com/Mocean_Ad_Request_API
         */
        public void AddCustomParameter(String customParamName, String value)
        {
            if (AdRequestParameters != null && customParamName != null)
            {
                AdRequestParameters.Add(customParamName, value);
            }
        }

        /**
         * Convenience method to add all custom parameters in one Map. Allows
         * setting of extra custom parameters to ad request.
         * 
         * @param customParamMap
         *            Map containing custom parameters
         * @see - For details of custom parameters refer
         *      http://developer.moceanmobile.com/Mocean_Ad_Request_API
         */
        public void AddCustomParametersMap(Dictionary<String, String> customParamMap)
        {
            if (AdRequestParameters != null && customParamMap != null)
            {
                foreach (var asset in customParamMap)
                    AdRequestParameters.Add(asset.Key, asset.Value);
            }
        }

        /**
         * Specifies the URL of the ad network. This defaults to Mocean's ad
         * network.
         * 
         * @param adNetworkURL
         *            URL of the ad server (ex: http://ads.moceanads.com/ad);
         */
        public void SetAdNetworkURL(String adNetworkURL)
        {
            NetworkUrl = adNetworkURL;
        }

        /**
         * Returns the currently configured ad network.
         * 
         * @return Currently configured ad network URL.
         */
        public String GetAdNetworkURL()
        {
            return NetworkUrl;
        }

        /**
         * Controls use of the internal browser. If used, a dialog will be used to
         * show a browser in the application for ads that are clicked on (that open
         * URLs). If not used an intent is started to invoke the system browser (or
         * whatever is configured to handle the intent).
         * 
         * @param useInternalBrowser
         *            true to use the internal browser, false to not use the
         *            internal browser.
         */
        public void SetUseInternalBrowser(bool useInternalBrowser)
        {
            this.UseInternalBrowser = useInternalBrowser;
        }

        /**
         * Returns the currently configured internal browser setting.
         * 
         * @return true if using the internal browser, false if not using the
         *         internal browser.
         */
        public bool GetUseInternalBrowser()
        {
            return UseInternalBrowser;
        }

        /**
         * Use this method to load your resource images.
         * 
         * @param url
         * @param imageView
         */
        public async Task LoadImage(String requestURL, Windows.UI.Xaml.Controls.Image imageView)
        {
            await ImageDownloader.LoadImage(requestURL, imageView);
        }

        /**
         * Updates ad.
         */
        public void Update()
        {
            InternalUpdate(false);
        }

        /**
         * This is an internal method used to update and make a new request to the
         * server. If defaulted is true then the id of the defaulted ad network is
         * added to the request so that this network will be removed from the
         * auction at PubMatic side.
         * 
         * @param defaulted
         *            denotes whether third party ad network defaulted
         */
        private void InternalUpdate(bool defaulted)
        {
            Dictionary<String, String> args = new Dictionary<String, String>();

            Reset();

            CancelUpdate();

            // Retrieve the User Agent
            if (string.IsNullOrEmpty(UserAgent))
            {
                this.UserAgentResume = true;
                DetermineUserAgent();
                return;
            }

            // Try to retrieve mobile network operator information
            try
            {
                IReadOnlyList<ConnectionProfile> connectionprofiles = NetworkInformation.GetConnectionProfiles();

                IEnumerator enumerator = connectionprofiles.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    ConnectionProfile profile = (ConnectionProfile)enumerator.Current;
                    String opetatorName = profile.ProfileName;

                    if ((opetatorName != null) && (opetatorName.Length > 3))
                    {
                        String mcc = profile.ProfileName.Substring(0, 3);
                        String mnc = profile.ProfileName.Substring(3);

                        args.Add(MASTNativeAdConstants.TELEPHONY_MCC, mcc);
                        args.Add(MASTNativeAdConstants.TELEPHONY_MNC, mnc);

                        Debug.WriteLine("Name : " + profile.ProfileName);

                        break;
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Unable to obtain mcc and mnc");
            }

            // Put all the user sent parameters in the request
            foreach (var adRequestParameters in AdRequestParameters)
                args.Add(adRequestParameters.Key, adRequestParameters.Value);

            // Don't allow these to be overridden.
            args.Add(MASTNativeAdConstants.REQUESTPARAM_SDK_VERSION, Defaults.SDK_VERSION);
            args.Add(MASTNativeAdConstants.REQUESTPARAM_COUNT, Defaults.NATIVE_REQUEST_COUNT);
            // Response type for Generic Json
            args.Add(MASTNativeAdConstants.REQUESTPARAM_KEY, Defaults.NATIVE_REQUEST_KEY);
            // Ad type for native.
            args.Add(MASTNativeAdConstants.REQUESTPARAM_TYPE, Defaults.NATIVE_REQUEST_AD_TYPE);
            args.Add(MASTNativeAdConstants.REQUESTPARAM_ZONE, Zone.ToString());

            try
            {
                if (mAdRequest != null)
                    mAdRequest.Cancel();

                mAdRequest = AdRequest.InitiateRequest(Defaults.NETWORK_TIMEOUT_SECONDS, NetworkUrl, UserAgent, args, GenerateNativeAssetRequestJson(),
                                new AdRequestServed(AdRequestServed),
                                new AdRequestError(AdRequestError),
                                new AdRequestFailed(AdRequestFailed),
                                true);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception encountered while generating ad request URL : " + e.Message);

                FireCallback(CallbackType.NativeFailed, e);
            }
        }

        private void DetermineUserAgent()
        {
            if (string.IsNullOrEmpty(UserAgent))
            {
                WebView uaBrowser = new WebView();
                uaBrowser.NavigationCompleted += UABrowser_NavigationCompleted;
                uaBrowser.NavigateToString("<!DOCTYPE html><html><head><script type='text/javascript'/></head><body></body></html>");
            }
        }

        async private void UABrowser_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            string ua = null;
            if (args.IsSuccess)
            {
                WebView webBrowser = sender as WebView;
                ua = await webBrowser.InvokeScriptAsync("eval", new string[] { "navigator.userAgent.toString()" });
            }

            if (string.IsNullOrEmpty(UserAgent))
                UserAgent = ua;

            ResumeInternalUpdate();
        }


        // Invoked after determining the user agent.
        private void ResumeInternalUpdate()
        {
            if (this.UserAgentResume == false)
                return;

            this.UserAgentResume = false;
            InternalUpdate(false);
        }

        private void CancelUpdate()
        {
            if (this.mAdRequest != null)
            {
                this.mAdRequest.Cancel();
                this.mAdRequest = null;
            }
        }

        // constants and listeners
        private enum TrackerType
        {
            IMPRESSION_TRACKER, CLICK_TRACKER
        }

        private void SendImpressionTrackers(TrackerType type)
        {
            switch(type)
            {
                case TrackerType.IMPRESSION_TRACKER :
                    
                    // Fire Impression Trackers here.
                    try
                    {
                        foreach (string trackerUrl in NativeAdDescriptor.GetNativeAdImpressionTrackers())
                        {
                            if (trackerUrl != null && trackerUrl.Length != 0)
                            {
                                NetworkHandler.StartWebRequest(trackerUrl, (result, exception) => {
                                    if (result != null)
                                    {
                                        Debug.WriteLine("------ Tracked  -" + trackerUrl + "Result " + result.StatusDescription + result.StatusCode);
                                    }

                                    if (exception != null)
                                    {
                                        Debug.WriteLine("------ Error -" + exception + " for URL  -" + trackerUrl);
                                    }
                                });
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }

                    break;

                case TrackerType.CLICK_TRACKER :

                    // Fire Click Trackers here.
                    try
                    {
                        foreach (string trackerUrl in NativeAdDescriptor.GetNativeAdClickTrackers())
                        {
                            if (trackerUrl != null && trackerUrl.Length != 0)
                            {
                                NetworkHandler.StartWebRequest(trackerUrl, (result, exception) => {
                                    if (result != null)
                                    {
                                        Debug.WriteLine("------ Tracked  -" + trackerUrl + "Result " + result.StatusDescription + result.StatusCode);
                                    }

                                    if (exception != null)
                                    {
                                        Debug.WriteLine("------ Error -" + exception + " for URL  -" + trackerUrl);
                                    }
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }

                    break;
            }
        }

        async public void RegisterTrackersForControl(UIElement control)
        {
            // Register the control for click trackers.
            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                control.Tapped += FireClickTracker;
            });

            /*
			 * Since publisher's view is ready to display. Send our impression
			 * tracker.
			 */
            if (!ImpressionTrackerSent)
            {
                SendImpressionTrackers(TrackerType.IMPRESSION_TRACKER);
                ImpressionTrackerSent = true;
            }
        }

        private void FireClickTracker(object sender, TappedRoutedEventArgs e)
        {
            /*
             * Since publisher's view is clicked. Send click trackers.
             */
            if (!ClickTrackerSent)
            {
                SendImpressionTrackers(TrackerType.CLICK_TRACKER);
                ClickTrackerSent = true;
            }

            OpenClickUrl(NativeAdDescriptor.GetClick());

            FireCallback(CallbackType.NativeAdClicked, null);
        }

        private void OpenClickUrl(string url)
        {
            if(UseInternalBrowser)
            {
                Frame frame;
                Page page = Context;
                frame = (page == null ? Window.Current.Content : page.Frame) as Frame;
                frame.Navigate(typeof(InternalBrowserPage), url);
            }
            else
            {
                // TODO : Launch Windows Default Web browser with the click url
            }
        }

        public void Reset()
        {
            if (mAdRequest != null)
            {
                mAdRequest.Cancel();
            }

            NativeAdDescriptor = null;

            ImpressionTrackerSent = false;
            ThirdPartyImpTrackerSent = false;
            ThirdPartyClickTrackerSent = false;
            ClickTrackerSent = false;
        }

        public void Destroy()
        {
            Reset();

            OverrideAdapter = false;
            RequestListener = null;
        }

        /**
         * Get the list of native assets.
         * 
         * @return List of {@link AssetResponse} if response contains any assets
         *         else returns null
         */
        public List<AssetResponse> GetNativeAssets()
        {
            if (NativeAdDescriptor != null
                    && NativeAdDescriptor.GetNativeAssetList() != null
                    && NativeAdDescriptor.GetNativeAssetList().Count > 0)
            {
                return NativeAdDescriptor.GetNativeAssetList();
            }
            return null;
        }

        /**
         * Get the javascript tracker received in native response. <br>
         * The javascript tracker is represented by 'jstracker' object in OpenRTB
         * Native ad specification.
         * <p>
         * <b>Note:</b> If jstracker is present, publisher should execute this
         * javascript at impression time (after onNativeAdReceived callback)
         * whenever possible.
         * 
         * @return JsTracker as string if present, else returns null.
         */
        public String GetJsTracker()
        {
            if (NativeAdDescriptor != null
                    && NativeAdDescriptor.GetJsTracker() != null)
            {
                return NativeAdDescriptor.GetJsTracker();
            }
            return null;
        }

        /**
         * Get the landing url whenever user clicks on the Native Ad
         * 
         * @return landing page url
         */
        public String GetClick()
        {
            if (NativeAdDescriptor != null)
            {
                return NativeAdDescriptor.GetClick();
            }

            return null;
        }

        /**
         * Returns the native ad response in json format.
         * 
         * @return
         */
        public String GetAdResponse()
        {
            if (NativeAdDescriptor != null)
            {
                return NativeAdDescriptor.GetNativeAdJSON();
            }

            return null;
        }

        private void LogEvent(String event1, LogLevel eventLevel)
        {
            //if (eventLevel.ordinal() > mLogLevel.ordinal())
            //return;

            if (mLogListener != null)
            {
                if (mLogListener.onLogEvent(this, event1, eventLevel))
                {
                    return;
                }
            }
        }

        private void FireCallback(CallbackType callbackType, Exception ex)
        {
            // Check if listener is set.
            if (RequestListener != null)
            {

                switch (callbackType)
                {
                    case MASTNativeAd.CallbackType.NativeReceived:
                        RequestListener.OnNativeAdReceived(this);
                        break;
                    case MASTNativeAd.CallbackType.NativeFailed:
                        RequestListener.OnNativeAdFailed(this, ex);
                        break;
                    case MASTNativeAd.CallbackType.NativeAdClicked:
                        RequestListener.OnNativeAdClicked(this);
                        break;
                }
            }
        }

        // Listeners for AdRequest.Listener start
        /*
         * Deprecating these methods as these methods are public and may be
         * accidentally called by the user causing unexpected behavior.
         * 
         * The access modifier will be changed to protected or default in the future
         * so that the user will not be able to see these methods and will not be
         * able to invoke these.
         */
        private void AdRequestFailed(AdRequest request, Exception exception)
        {
            LogEvent("Ad request failed: " + exception, LogLevel.Error);

            FireCallback(CallbackType.NativeFailed, exception);
        }

        private void AdRequestError(AdRequest request, string errorCode, string errorMessage)
        {

            if (request != mAdRequest)
                return;

            FireCallback(CallbackType.NativeFailed, new Exception(errorMessage));

            LogLevel logLevel = LogLevel.Error;
            if ("404".Equals(errorCode))
            {
                logLevel = LogLevel.Debug;
            }

            LogEvent("Error response from server.  Error code: " + errorCode
                    + ". Error message: " + errorMessage, logLevel);

        }

        private void AdRequestCompleted(AdRequest adRequest, AdDescriptor adDescriptor)
        {
            if (adRequest != mAdRequest)
                return;

            if (adDescriptor != null && adDescriptor is NativeAdDescriptor)
            {
                NativeAdDescriptor = (NativeAdDescriptor)adDescriptor;

                // If it is the native response, then pass the native response
                // Set all the properties for native ad
                FireCallback(CallbackType.NativeReceived, null);
            }
            else
            {
                FireCallback(CallbackType.NativeFailed, new Exception("Incorrect response received"));
            }
        }

        private void AdRequestServed(AdRequest request, System.Net.HttpWebResponse response)
        {
            System.IO.Stream responseStream = response.GetResponseStream();

            AdDescriptor adDescriptor = AdDescriptor.parseNativeResponse(responseStream);

            if (request != mAdRequest)
                return;

            if (adDescriptor != null && adDescriptor is NativeAdDescriptor)
            {
                NativeAdDescriptor = (NativeAdDescriptor)adDescriptor;

                // If it is the native response, then pass the native response
                // Set all the properties for native ad
                FireCallback(CallbackType.NativeReceived, null);
            }
            else
            {
                FireCallback(CallbackType.NativeFailed, new Exception("Incorrect response received"));
            }
        }

        private String GenerateNativeAssetRequestJson()
        {

            String nativeRequestPostData = MASTNativeAdConstants.REQUEST_NATIVE_EQ_WRAPPER;

            JsonObject nativeObj = new JsonObject();
            nativeObj.Add(MASTNativeAdConstants.REQUEST_VER, JsonValue.CreateStringValue(MASTNativeAdConstants.REQUEST_VER_VALUE_1));

            JsonArray assetsArray = new JsonArray();

            JsonObject assetObj;
            JsonObject titleObj;
            JsonObject imageObj;
            JsonObject dataObj;

            for (int i = 0; i < RequestedNativeAssets.Count; i++)
            {
                AssetRequest assetRequest;

                assetRequest = RequestedNativeAssets[i];

                if (assetRequest != null)
                {
                    assetObj = new JsonObject();
                    assetObj.Add(MASTNativeAdConstants.ID_STRING, JsonValue.CreateNumberValue(assetRequest.GetAssetId()));
                    assetObj.Add(MASTNativeAdConstants.REQUEST_REQUIRED, JsonValue.CreateNumberValue((assetRequest.IsRequired() ? 1 : 0)));

                    if (assetRequest is TitleAssetRequest)
                    {
                        if (((TitleAssetRequest)assetRequest).Length > 0)
                        {
                            titleObj = new JsonObject();
                            titleObj.Add(MASTNativeAdConstants.REQUEST_LEN, JsonValue.CreateNumberValue(((TitleAssetRequest)assetRequest).Length));
                            assetObj.Add(MASTNativeAdConstants.REQUEST_TITLE, titleObj);
                        }
                        else
                        {
                            assetObj = null;
                            Debug.WriteLine("'length' parameter is mandatory for title asset");
                        }
                    }
                    else if (assetRequest is ImageAssetRequest)
                    {
                        imageObj = new JsonObject();

                        if (((ImageAssetRequest)assetRequest).GetImageType() != null)
                        {
                            imageObj.Add(MASTNativeAdConstants.REQUEST_TYPE, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).GetTypeId()));
                        }
                        if (((ImageAssetRequest)assetRequest).Width > 0)
                        {
                            imageObj.Add(MASTNativeAdConstants.NATIVE_IMAGE_W, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).Width));
                        }
                        if (((ImageAssetRequest)assetRequest).Height > 0)
                        {
                            imageObj.Add(MASTNativeAdConstants.NATIVE_IMAGE_H, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).Height));
                        }

                        assetObj.Add(MASTNativeAdConstants.REQUEST_IMG, imageObj);
                    }
                    else if (assetRequest is DataAssetRequest)
                    {
                        dataObj = new JsonObject();
                        if (((DataAssetRequest)assetRequest).DataAssetType != null)
                        {
                            dataObj.Add(MASTNativeAdConstants.REQUEST_TYPE, JsonValue.CreateNumberValue(((DataAssetRequest)assetRequest).GetTypeId()));

                            if (((DataAssetRequest)assetRequest).Length > 0)
                            {
                                dataObj.Add(MASTNativeAdConstants.REQUEST_LEN, JsonValue.CreateNumberValue(((DataAssetRequest)assetRequest).Length));
                            }

                            assetObj.Add(MASTNativeAdConstants.REQUEST_DATA, dataObj);
                        }
                        else
                        {
                            assetObj = null;
                            Debug.WriteLine("'type' parameter is mandatory for data asset");
                        }
                    }
                    if (assetObj != null)
                    {
                        assetsArray.Add(assetObj);
                    }
                }
            }

            nativeObj.Add(MASTNativeAdConstants.NATIVE_ASSETS_STRING, assetsArray);

            nativeRequestPostData += nativeObj.ToString();

            return nativeRequestPostData;

        }

        public class Image
        {
            String url = null;
            int width = 0;
            int height = 0;

            public Image(String url)
            {
                this.url = url;
            }

            public Image(String url, int width, int height)
            {
                this.url = url;
                this.width = width;
                this.height = height;
            }

            public String GetUrl()
            {
                return url;
            }

            int GetWidth()
            {
                return width;
            }

            int GetHeight()
            {
                return height;
            }

            /**
             * Parses the JSON and returns the object of {@link Image}, null
             * otherwise.
             * 
             * @param json
             * @return object of {@link Image}, else null if the parsing fails
             */
            public static Image GetImage(JsonObject jsonImage)
            {
                Image image = null;

                if (jsonImage != null && jsonImage[MASTNativeAdConstants.RESPONSE_URL].GetString() != null)
                {
                    String url = jsonImage[MASTNativeAdConstants.RESPONSE_URL].GetString();
                    double width = jsonImage[MASTNativeAdConstants.NATIVE_IMAGE_W].GetNumber();
                    double height = jsonImage[MASTNativeAdConstants.NATIVE_IMAGE_H].GetNumber();
                    image = new Image(url, Convert.ToInt32(width), Convert.ToInt32(height));
                }

                return image;
            }
        }

    }
}
