﻿namespace MASTNativeAdView
{
    public class ImageAssetRequest : AssetRequest
    {
        /** Image Type */
        private ImageAssetTypes ImageType;

        /** Image width */
        public int Width;

        /** Image height */
        public int Height;

        public ImageAssetTypes GetImageType()
        {
            return ImageType;
        }

        public void SetImageType(ImageAssetTypes imageType)
        {
            this.ImageType = imageType;
        }

        public int GetWidth()
        {
            return Width;
        }

        public void SetWidth(int width)
        {
            this.Width = width;
        }

        public int GetHeight()
        {
            return Height;
        }

        public void SetHeight(int height)
        {
            this.Height = height;
        }

        public int GetTypeId()
        {
            if (ImageType == ImageAssetTypes.Icon)
                return 1;
            else if (ImageType == ImageAssetTypes.Logo)
                return 2;
            else if (ImageType == ImageAssetTypes.Main)
                return 3;
            else
                return -1;
        }
    }
}
