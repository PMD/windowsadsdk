﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Diagnostics;

namespace MASTNativeAdView
{
    public class ImageDownloader
    {
        private System.Net.HttpWebRequest WebRequest = null;
        private String RequestURL;
        private Image ImageView;

        public ImageDownloader(String requestURL,Image imageView)
        {
            this.RequestURL = requestURL;
            this.ImageView = imageView;

            CreateGetRequest();
        }

        public void CreateGetRequest()
        {
            this.WebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(this.RequestURL);

            IAsyncResult result = this.WebRequest.BeginGetResponse(new AsyncCallback(OnImageReceived), null);
        }

        public async static Task LoadImage(String requestURL,Image imageView)
        {
            new ImageDownloader(requestURL,imageView);
        }

        private async void OnImageReceived(IAsyncResult ar)
        {
            try
            {
                System.Net.HttpWebResponse Response = (System.Net.HttpWebResponse)this.WebRequest.EndGetResponse(ar);

                if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    // Get Image Stream
                    Stream responseStream = Response.GetResponseStream();

                    // Convert Image Stream to Memory Stream
                    var memStream = new MemoryStream();
                    await responseStream.CopyToAsync(memStream);
                    memStream.Position = 0;

                    // Sets the Source of Image in UI Thread
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            // Removing Source
                            ImageView.Source = null;
                            var bitmap = new BitmapImage();
                            bitmap.SetSource(memStream.AsRandomAccessStream());
                            ImageView.Source = bitmap;
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error while parsing image : " + ex.Message);
            }
        }
    }
}
