﻿namespace MASTNativeAdView
{
    public class AssetResponse
    {
        /** Asset Id */
        public int AssetId;

        public int GetAssetId()
        {
            return AssetId;
        }

        public void SetAssetId(int assetId)
        {
            this.AssetId = assetId;
        }

    }
}
